**FREE

///
// Hello World Example on IBM i
// 
// This program shows how to create an open document spreadsheet on IBM i
// in RPG with libxlsxwriter service program.
//
// @author Mihael Schmidt
// @date 14.03.2018
///


ctl-opt dftactgrp(*no) actgrp(*caller) bnddir('LIBXLSX/LIBXLSX');


/include 'libxlsxwriter/xlsxwriter_h.rpgle'


main();
*inlr = *on;


dcl-proc main;

  dcl-s workbook pointer;
  dcl-s worksheet pointer;
  dcl-s error int(10);

  workbook = workbook_new('test1.xlsx');

  worksheet = workbook_add_worksheet(workbook : 'Test Worksheet');
  if (worksheet = *null);
    dsply 'Worksheet is NULL!';
  endif;

  worksheet_write_number(worksheet : 0 : 0 : 123 : *null);
  worksheet_write_number(worksheet : 1 : 0 : 123.456 : *null);
  
  worksheet_write_string(worksheet : 0 : 1 : 'Hello IBM i!' : *null);

  workbook_close(workbook);

end-proc;
