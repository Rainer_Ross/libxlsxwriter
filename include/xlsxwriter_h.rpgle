**FREE

/if not defined(LIBXLSXWRITER)
/define LIBXLSXWRITER

//
// Templates
//

dcl-ds lxw_workbook_options qualified template;
  constant_memory int(3);
  dummy char(15);
  tmpdir pointer;
end-ds;


//
// Constants
//

dcl-c LXW_DEFAULT_ROW_HEIGHT 15,0;
dcl-c LXW_DEFAULT_COLUMN_WIDTH 8,43;

dcl-c LXW_BORDER_NONE 1;
dcl-c LXW_BORDER_THIN 2;
dcl-c LXW_BORDER_MEDIUM 3;
dcl-c LXW_BORDER_DASHED 4;
dcl-c LXW_BORDER_DOTTED 5;
dcl-c LXW_BORDER_THICK 6;
dcl-c LXW_BORDER_DOUBLE 7;
dcl-c LXW_BORDER_HAIR 8;
dcl-c LXW_BORDER_MEDIUM_DASHED 9;
dcl-c LXW_BORDER_DASH_DOT 10;
dcl-c LXW_BORDER_MEDIUM_DASH_DOT 11;
dcl-c LXW_BORDER_DASH_DOT_DOT 12;
dcl-c LXW_BORDER_MEDIUM_DASH_DOT_DOT 13;
dcl-c LXW_BORDER_SLANT_DASH_DOT 14;

dcl-c LXW_PATTERN_NONE 0;
dcl-c LXW_PATTERN_SOLID 1;
dcl-c LXW_PATTERN_MEDIUM_GRAY 2;
dcl-c LXW_PATTERN_DARK_GRAY 3;
dcl-c LXW_PATTERN_LIGHT_GRAY 4;
dcl-c LXW_PATTERN_DARK_HORIZONTAL 5;
dcl-c LXW_PATTERN_DARK_VERTICAL 6;
dcl-c LXW_PATTERN_DARK_DOWN 7;
dcl-c LXW_PATTERN_DARK_UP 8;
dcl-c LXW_PATTERN_DARK_GRID 9;
dcl-c LXW_PATTERN_DARK_TRELLIS 10;
dcl-c LXW_PATTERN_LIGHT_HORIZONTAL 11;
dcl-c LXW_PATTERN_LIGHT_VERTICAL 12;
dcl-c LXW_PATTERN_LIGHT_DOWN 13;
dcl-c LXW_PATTERN_LIGHT_UP 14;
dcl-c LXW_PATTERN_LIGHT_GRID 15;
dcl-c LXW_PATTERN_LIGHT_TRELLIS 16;
dcl-c LXW_PATTERN_GRAY_125 17;
dcl-c LXW_PATTERN_GRAY_625 18;

dcl-c LXW_COLOR_BLACK 0;
dcl-c LXW_COLOR_BLUE 255;
dcl-c LXW_COLOR_BROWN 8388608;
dcl-c LXW_COLOR_CYAN 65535;
dcl-c LXW_COLOR_GRAY 8421504;
dcl-c LXW_COLOR_GREEN 32768;
dcl-c LXW_COLOR_LIME 65280;
dcl-c LXW_COLOR_MAGENTA 16711935;
dcl-c LXW_COLOR_NAVY 128;
dcl-c LXW_COLOR_ORANGE 16737792;
dcl-c LXW_COLOR_PINK 16711935;
dcl-c LXW_COLOR_PURPLE 8388736;
dcl-c LXW_COLOR_RED 16711680;
dcl-c LXW_COLOR_SILVER 12632256;
dcl-c LXW_COLOR_YELLOW 16776960;
dcl-c LXW_COLOR_WHITE 16777215;

dcl-c LXW_ALIGN_NONE 0;
dcl-c LXW_ALIGN_LEFT 1;
dcl-c LXW_ALIGN_CENTER 2;
dcl-c LXW_ALIGN_RIGHT 3;
dcl-c LXW_ALIGN_FILL 4;
dcl-c LXW_ALIGN_JUSTIFY 5;
dcl-c LXW_ALIGN_CENTER_ACROSS 6;
dcl-c LXW_ALIGN_DISTRIBUTED 7;
dcl-c LXW_ALIGN_VERTICAL_TOP 8;
dcl-c LXW_ALIGN_VERTICAL_BOTTOM 9;
dcl-c LXW_ALIGN_VERTICAL_CENTER 10;
dcl-c LXW_ALIGN_VERTICAL_JUSTIFY 11;
dcl-c LXW_ALIGN_VERTICAL_DISTRIBUTED 12;

dcl-c LXW_FORMAT_SUPERSCRIPT 1;
dcl-c LXW_FORMAT_SUBSCRIPT 2;

dcl-c LXW_UNDERLINE_SINGLE 1;
dcl-c LXW_UNDERLINE_DOUBLE 2;
dcl-c LXW_UNDERLINE_SINGLE_ACCOUNTING 3;
dcl-c LXW_UNDERLINE_DOUBLE_ACCOUNTING 4;


//
// Prototypes
//
dcl-pr workbook_new pointer extproc('workbook_new');
  name pointer value options(*string);
end-pr;
dcl-pr workbook_new_opt pointer extproc('workbook_new_opt');
  name pointer value options(*string);
  options likeds(lxw_workbook_options);
end-pr;

dcl-pr workbook_add_worksheet pointer extproc('workbook_add_worksheet');
  workbook pointer value;
  name pointer value options(*string);
end-pr;

dcl-pr workbook_close int(10) extproc('workbook_close');
  workbook pointer value;
end-pr;

dcl-pr workbook_add_format pointer extproc('workbook_add_format');
  workbook pointer value;
end-pr;

dcl-pr worksheet_write_string int(10) extproc('worksheet_write_string');
  worksheet pointer value;
  row int(10) value;
  column int(10) value;
  string pointer value options(*string);
  format pointer value;
end-pr;

dcl-pr worksheet_write_formula int(10) extproc('worksheet_write_formula');
  worksheet pointer value;
  row int(10) value;
  column int(10) value;
  string pointer value options(*string);
  format pointer value;
end-pr;

dcl-pr worksheet_write_boolean int(10) extproc('worksheet_write_boolean');
  worksheet pointer value;
  row int(10) value;
  column int(10) value;
  value int(10) value;
  format pointer value;
end-pr;

dcl-pr worksheet_write_url int(10) extproc('worksheet_write_url');
  worksheet pointer value;
  row int(10) value;
  column int(10) value;
  url pointer value options(*string);
  format pointer value;
end-pr;

dcl-pr worksheet_write_blank int(10) extproc('worksheet_write_blank');
  worksheet pointer value;
  row int(10) value;
  column int(10) value;
  format pointer value;
end-pr;

dcl-pr worksheet_set_row int(10) extproc('worksheet_set_row');
  worksheet pointer value;
  row int(10) value;
  height float(8) value;
  format pointer value;
end-pr;

dcl-pr worksheet_set_row_opt int(10) extproc('worksheet_set_row_opt');
  worksheet pointer value;
  row int(10) value;
  height float(8) value;
  format pointer value;
  options pointer value;
end-pr;

dcl-pr worksheet_set_column int(10) extproc('worksheet_set_column');
  worksheet pointer value;
  columnFrom int(10) value;
  columnTo int(10) value;
  width float(8) value;
  format pointer value;
end-pr;

dcl-pr worksheet_set_column_opt int(10) extproc('worksheet_set_column_opt');
  worksheet pointer value;
  columnFrom int(10) value;
  columnTo int(10) value;
  width float(8) value;
  format pointer value;
  options pointer value;
end-pr;

dcl-pr worksheet_merge_range int(10) extproc('worksheet_merge_range');
  worksheet pointer value;
  rowFrom int(10) value;
  columnFrom int(10) value;
  rowTo int(10) value;
  columnTo int(10) value;
  string pointer value options(*string);
  format pointer value;
end-pr;

dcl-pr worksheet_freeze_panes extproc('worksheet_freeze_panes');
  worksheet pointer value;
  row int(10) value;
  column int(10) value;
end-pr;

dcl-pr worksheet_split_panes extproc('worksheet_split_panes');
  worksheet pointer value;
  vertical float(8) value;
  horizontal float(8) value;
end-pr;

dcl-pr worksheet_set_selection extproc('worksheet_set_selection');
  worksheet pointer value;
  rowFrom int(10) value;
  columnFrom int(10) value;
  rowTo int(10) value;
  columnTo int(10) value;
end-pr;

dcl-pr worksheet_set_landscape extproc('worksheet_set_landscape');
  worksheet pointer value;
end-pr;

dcl-pr worksheet_set_portrait extproc('worksheet_set_portrait');
  worksheet pointer value;
end-pr;

dcl-pr worksheet_set_page_view extproc('worksheet_set_page_view');
  worksheet pointer value;
end-pr;

dcl-pr worksheet_hide_zero extproc('worksheet_hide_zero');
  worksheet pointer value;
end-pr;

dcl-pr worksheet_write_number int(10) extproc('worksheet_write_number');
  worksheet pointer value;
  row int(10) value;
  column int(10) value;
  number float(8) value;
  format pointer value;
end-pr;

dcl-pr format_set_font_name extproc('format_set_font_name');
  format pointer value;
  fontname pointer value options(*string);
end-pr;

dcl-pr format_set_font_size extproc('format_set_font_size');
  format pointer value;
  size float(8) value;
end-pr;

dcl-pr format_set_font_color extproc('format_set_font_color');
  format pointer value;
  color int(10) value;
end-pr;

dcl-pr format_set_bold extproc('format_set_bold');
  format pointer value;
end-pr;

dcl-pr format_set_italic extproc('format_set_italic');
  format pointer value;
end-pr;

dcl-pr format_set_underline extproc('format_set_underline');
  format pointer value;
  style uns(3) value;
end-pr;

dcl-pr format_set_font_strikeout extproc('format_set_font_strikeout');
  format pointer value;
end-pr;

dcl-pr format_set_font_script extproc('format_set_font_script');
  format pointer value;
  style uns(3) value;
end-pr;

dcl-pr format_set_num_format extproc('format_set_num_format');
  format pointer value;
  numberFormat pointer value options(*string);
end-pr;

dcl-pr format_set_align extproc('format_set_align');
  format pointer value;
  alignment uns(3) value;
end-pr;

dcl-pr format_set_pattern extproc('format_set_pattern');
  format pointer value;
  patternIndex uns(3) value;
end-pr;

dcl-pr format_set_bg_color extproc('format_set_bg_color');
  format pointer value;
  color int(10) value;
end-pr;

dcl-pr format_set_fg_color extproc('format_set_fg_color');
  format pointer value;
  color int(10) value;
end-pr;

dcl-pr format_set_border extproc('format_set_border');
  format pointer value;
  style uns(3) value;
end-pr;

dcl-pr format_set_text_wrap extproc('format_set_text_wrap');
  format pointer value;
end-pr;

/endif
